use std::io::Cursor;

mod addressbook_capnp {
    include!(concat!(env!("OUT_DIR"), "/addressbook_capnp.rs"));
}

fn pretend_recv_person_good() ->
capnp::message::TypedReader<
    capnp::serialize::OwnedSegments,
    addressbook_capnp::person::Owned,
>
{
    let bytes = b"";
    let msg_cursor = Cursor::new(bytes);

    let mut options = capnp::message::ReaderOptions::new();
    options.nesting_limit(16).traversal_limit_in_words(Some(128));

    let deser = capnp::serialize::read_message(msg_cursor, options).unwrap();

    let reader: capnp::message::TypedReader<
        capnp::serialize::OwnedSegments,
        addressbook_capnp::person::Owned,
    > = capnp::message::TypedReader::new(deser);

    reader
}

fn pretend_recv_person_bad<'a>() -> addressbook_capnp::person::Reader<'a> {
    let bytes = b"";
    let msg_cursor = Cursor::new(bytes);

    let mut options = capnp::message::ReaderOptions::new();
    options.nesting_limit(16).traversal_limit_in_words(Some(128));

    let deser = capnp::serialize::read_message(msg_cursor, options).unwrap();

    let reader: capnp::message::TypedReader<
        capnp::serialize::OwnedSegments,
        addressbook_capnp::person::Owned,
    > = capnp::message::TypedReader::new(deser);

    reader.get().unwrap()
}

fn main() {
    let msg_typed = pretend_recv_person_good();
    let msg = msg_typed.get().unwrap();
    let msg_kind = msg.get_name().unwrap();
    assert_eq!(msg_kind, "");

    let msg = pretend_recv_person_bad();
    let msg_kind = msg.get_name().unwrap();
    assert_eq!(msg_kind, "");
}
